import axios from 'axios';

const api = axios.create({
    baseURL: "http://" + process.env.REACT_APP_API_BACK,
    timeout: 5000,
    headers: {
        "X-Requested-With": "XMLHttpRequest",
    },
});

export const getPersonUser = async () => {
    let body={
        idPersona:1,
    }
    const response = await api({
        method: "post",
        url: "/api/persona/getOne",
        data:body
    });
    console.log("RESPONSE", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const getCompanyUser = async () => {
    let body={
        idOrganizacion:1,
    }
    const response = await api({
        method: "post",
        url: "/api/organizacion/getOne",
        data:body
    });
    console.log("RESPONSE", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const categoryList = async () => {
    const response = await api({
        method: "post",
        url: "/api/categoria/list",
    });
    console.log("RESPONSE", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const problemList = async (id) => {
    let body={
        idCategoria:id,
    }
    const response = await api({
        method: "post",
        url: "/api/problematica-by-categoria/lista",
        data:body,
    });
    console.log("RESPONSE", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};
        
export const getProject = async (idPropuesta) => {
    let body={
        idProyecto: idPropuesta
    }
    const response = await api({
        method: "post",
        url: "/api/proyecto/getOne",
        data:body,
    });
    console.log("RESPONSE", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const proyectListTop = async (id) => {
    let body={
    }
    const response = await api({
        method: "post",
        url: "/api/mejores-proyectos",
        data:body,
    });
    console.log("RESPONSE", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const proyectByCategoryList = async (id) => {
    let body={
        idCategoria:id,
    }
    const response = await api({
        method: "post",
        url: "/api/mejores-proyectos-por-categorias",
        data:body,
    });
    console.log("RESPONSE", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const proyectByProblematicList = async (id) => {
    let body={
        idProblematica:id,
    }
    const response = await api({
        method: "post",
        url: "/api/problematica/proyecto/lista",
        data:body,
    });
    console.log("RESPONSE", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const votar = async (id) => {
    let body={
        idProyecto:id,
    }
    const response = await api({
        method: "post",
        url: "/api/proyecto/addVote",
        data:body,
    });
    console.log("RESPONSE", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const projectRegister = async (idProblematica, idPersona, nombre, descripcion, urlVideo,recaudacionEsperada) => {
    let body={
        idProblematica:idProblematica,
        idPersona:idPersona,
        nombre:nombre,
        descripcion:descripcion,
        urlVideo:urlVideo,
        recaudacionEsperada:recaudacionEsperada,
    }
    console.log(body);
    const response = await api({
        method: "post",
        url: "/api/proyecto/addOne",
        data:body,
    });
    // const response=body;
    console.log("RESPONSE", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const donar = async (idProyecto, idPersona, donacion) => {
    let body={
        idProyecto:idProyecto,
        idPersona:idPersona,
        donacion:parseInt(donacion)
    }

    const response = await api({
        method: "post",
        url: "/api/proyecto/addDonacion",
        data:body,
    });

    console.log("RESPONSE", response);
    if (response.data.code >= 200 && response.data.code < 300) return response.data;
    else return null;
}

export const financiar = async (idProyecto,idOrganizacion) => {
    let body={
        idProyecto:idProyecto,
        idOrganizacion:idOrganizacion
    }

    const response = await api({
        method: "post",
        url: "/api/proyecto/addParticinador",
        data:body,
    });

    console.log("RESPONSE", response);
    if (response.data.code >= 200 && response.data.code < 300) return response.data;
    else return null;
}