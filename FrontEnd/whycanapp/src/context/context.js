import React, { createContext, useState } from "react";

export const MainContext=createContext();

export const MainProvider=(props)=>{
    const[usuario,setUsuario]=useState(null);
    const[tipoUsuario,setTipoUsuario]=useState(null);
    const[selectedProblematic,setSelectedProblematic]=useState(null);
    const[openProbModal,setOpenProbModal]=useState(null);

    const obj={
        usuario,
        setUsuario,
        tipoUsuario,
        setTipoUsuario,
        selectedProblematic,
        setSelectedProblematic,
        openProbModal,
        setOpenProbModal,
        
    }

    return(
        <MainContext.Provider value={obj}>{props.children}</MainContext.Provider>
    );
    
};