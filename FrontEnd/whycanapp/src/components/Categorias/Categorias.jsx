import React, { useEffect, useState, component } from 'react'
import { Grid, Card, CardActionArea,
    CardContent, CardMedia, Typography } from '@material-ui/core'
import Swal from 'sweetalert2'
import { categoryList } from '../../api/services'
import { useStyles } from './Categorias.module';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import fondoPersonas from '../../assets/img/Personas.png';

export default function Categorias(props) {
    const classes = useStyles();
    const [categorias, setCategorias] = useState([]);

    useEffect(() => {
        async function getData() {
            const datos = await categoryList();
            if (datos) {
                setCategorias(datos.body)
            } else {
                await Swal.fire({
                    title: "Error",
                    text: datos.message,
                    icon: "error",
                });
            }
        }
        getData();
    }, []);

const goToCategory=(id)=>{
    props.history.push({
        pathname:"/proyectos",
        state:{idCategoria:id},
    });
};

    return (
        <div>
            <Grid container direction="column">
                <Grid item style={{marginLeft:20,marginTop:10}}>
                    <h1>Seleccione una Categoría</h1>
                  

                </Grid>
            </Grid>
        <Grid container alignContent="stretch" justify="center" className={classes.flex}>
            {categorias.map((x) => (
                <Grid item xs={12} sm ={6} md={3} key={x.id}>
                    <Card className={classes.flex}>
                        <CardActionArea className={classes.flex}>
                            <CardMedia
                                component="img"
                                alt=""
                                image={x.urlImagen}
                                title={x.nombre}
                                onClick={()=>goToCategory(x.id)}
                            />
                            <CardContent>
                            <ExpansionPanel>
                                <ExpansionPanelSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id={x.id}
                                >
                                <Grid container direction="column" alignItems="center" justify="flex-end">
                                    <Grid item>
                                        <Typography gutterBottom variant="h6" component="h2">
                                            {x.nombre}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                <Grid container>
                                    <Grid item>
                                        <Typography>
                                            {x.descripcion}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>
            ))}
        </Grid>
        </div>
    )
}
