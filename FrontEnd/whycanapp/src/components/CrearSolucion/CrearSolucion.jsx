import React, { useEffect, useState } from 'react'
import { Grid, Card, CardActionArea,
    CardContent, CardMedia, Typography, Dialog, DialogTitle, DialogContent, DialogContentText, Button, TextField, DialogActions } from '@material-ui/core'
import Swal from 'sweetalert2'
import { projectRegister } from '../../api/services'
import { useStyles } from './CrearSolucion.module'

export default function CrearSolucion() {
    const [open, setOpen] = React.useState(false);
    const [solucion, setSolucion] = useState([]);

    const abrir = () => {
        setOpen(true);
    };

    const cerrar = () => {
        setOpen(false);
    };

    async function registrarSolucion(idProblematica, idPersona, nombre, descripcion, urlVideo) {
        const datos = await projectRegister(idProblematica, idPersona, nombre, descripcion, urlVideo);
        if (datos) {
            //console.log(datos)
        } else {
            await Swal.fire({
                title: "Error",
                text: datos.message,
                icon: "error",
            });
        }
    }

    const registrar = () => {
        if(!solucion.videoLink){
            solucion.videoLink=""
        }
        registrarSolucion(1, 1, solucion.titulo, solucion.descripcion, solucion.videoLink);
        setOpen(false);
    };

    return (
        <div>
            <Button variant="outlined" color="primary" onClick={abrir}>
                Open form dialog
            </Button>
            <Dialog open={open} onClose={cerrar}>
                <DialogTitle id="form-dialog-title">Registrar Proyecto</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Registre su proyecto en este formulario. Puede agregar un video promocionando el proyecto.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Título del proyecto"
                        fullWidth
                        onChange={(e) => {
                            let copy = {...solucion};
                            copy.titulo=e.target.value;
                            setSolucion(copy);
                        }}
                    />
                    <TextField
                        margin="dense"
                        id="name"
                        label="Descripción del proyecto"
                        rows={4}
                        fullWidth
                        multiline
                        onChange={(e) => {
                            let copy = {...solucion};
                            copy.descripcion=e.target.value;
                            setSolucion(copy);
                        }}
                    />
                    <TextField
                        margin="dense"
                        id="name"
                        label="Link de video"
                        fullWidth
                        onChange={(e) => {
                            let copy = {...solucion};
                            copy.videoLink=e.target.value;
                            setSolucion(copy);
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={cerrar} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={registrar} color="primary">
                        Registrar
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}