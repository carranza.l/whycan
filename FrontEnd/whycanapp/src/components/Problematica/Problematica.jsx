import React, { useEffect, useState} from 'react';
import Swal from 'sweetalert2';
import {
    Toolbar,
    Typography,
    CssBaseline,
    Container,
    Grid,
    Button,
    Card,
    CardMedia,
    CardContent,
    CardActions,
    CardActionArea,
    Link
} from "@material-ui/core";
import { useStyles } from "./Problematica.module";
import fondoPersonas from '../../assets/img/Personas.png';

const sections = [
    { title: '#Costa', url: '#' },
    { title: '#Lima', url: '#' },
    { title: '#Categoria1', url: '/categorias' }
];

export default function Problematica(props) {
  const classes = useStyles();
  const [propuestas, setPropuestas] = useState([]);
  
const goToProject=(id)=>{
    props.history.push({
        pathname:"/propuesta",
        state:{idPropuesta:id},
    });
};

  return (
    <React.Fragment>
      <CssBaseline />
      <main>
        <div className={classes.heroContent}>
            <Grid container direction="row" alignContent="stretch" justify="center" className={classes.flex}>
                <Grid item xs={12} className={classes.flex}>
                    <Typography component="h2" variant="h2">
                    Titulo
                    </Typography>
                </Grid>
                <Grid item xs={12} className={classes.flex}>
                    {sections.map((section) => (
                        <Link
                            color="inherit"
                            noWrap
                            key={section.title}
                            variant="body2"
                            href={section.url}
                            className={classes.toolbarLink}
                        >
                            <Button className={classes.peruButton}>{section.title}</Button>
                        </Link>
                    ))}
                </Grid>
                <Grid item xs={12} justify="center" align="center" justify-content="center" className={classes.flex}>
                    <img src={fondoPersonas} alt="fondoPersonas"></img>
                </Grid>
                <Grid item xs={12} className={classes.flex}>
                    <Typography component="h9" variant="h9" color="textSecondary">
                        Fecha
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography component="h9" variant="h9" color="textPrimary" paragraph>
                        Descripcion                  
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                <Grid container alignContent="stretch" justify="center" className={classes.flex}>
                    {propuestas.map((x) => (
                        <Grid item xs={6} sm ={4} md={3} key={x.id}>
                            <Card className={classes.flex} >
                                <CardActionArea onClick={()=>goToProject(x.id)} className={classes.flex}>
                                    <CardMedia
                                        component="img"
                                        alt="fondo Personas"
                                        image={fondoPersonas}
                                        title={x.nombre}
                                    />
                                    <CardContent>
                                        <Grid container direction="column" alignItems="center" justify="flex-end">
                                            <Grid item>
                                                <Typography gutterBottom variant="h5" component="h2">
                                                    {x.nombre}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
                </Grid>
            </Grid>
        </div>
      </main>
    </React.Fragment>
  );
}