import React from 'react'
import { makeStyles } from '@material-ui/core'

export const useStyles=makeStyles((theme)=>({
    root: {
        width:"100%",
        height:"100%",
        maxWidth: "100%",
        maxHeight:"100%",
        marginTop: "40px",
        marginLeft: "50px",
        marginRight: "50px",
        paddingTop: "50px",
        paddingRight: "30px",
        paddingBottom: "50px",
        paddingLeft: "30px",
        backgroundColor: "white"
    },
    button: {
        marginLeft: "10px"
    },
    detailsContainer: {
        marginTop: "30px",
        marginBottom: "30px",
    },
    detailLine: {
        marginTop: "20px",
    },
    peruButton:{
        marginLeft: "10px",
        color: 'white',
        backgroundColor: '#9a031e',
        '&:hover':{
          color: 'black',
          backgroundColor: 'white'
        }      
    },
}))
