import React, { useEffect, useState, useContext } from 'react';
import { Grid, Card, CardActionArea,
    CardContent, CardMedia, Typography, Button, TextField,
    Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import Swal from 'sweetalert2';
import { useStyles } from './Proyecto.module';
import { MainContext } from '../../context/context';
import { getProject, votar, donar, financiar } from '../../api/services';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';

export default function Proyecto(props) {
    const context = useContext(MainContext);
    const classes = useStyles();
    const [sourcevid,setSourcevid] = useState([]);
    const [propuesta,setPropuesta] = useState([]);
    const [montoDonacion,setMontoDonacion] = useState([]);
    const [isOpenFinanciar, setIsOpenFinanciar] = React.useState(false);
    const [isOpenDonar, setIsOpenDonar] = React.useState(false);
    const [isOpenProponerContacto, setIsOpenProponerContacto] = React.useState(false);
    const [isOpenRespuesta, setIsOpenRespuesta] = React.useState(false);
    const [isOpenRespuesta2, setIsOpenRespuesta2] = React.useState(false);
    const [voto, setVoto] = useState("");
    const idPropuesta=props.location.state.idPropuesta;
    console.log("EL ID PROPUESTA: ",idPropuesta);
    let idVideo=""

    function youtube_parser(url){
        let regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
        let match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }

    useEffect(() => {
        async function getData() {
            const datos = await getProject(idPropuesta);
            if (datos.body) {
                if(datos.body.enlaceVideo!==null){
                    idVideo=youtube_parser(datos.body.enlaceVideo)
                    
                    setSourcevid(idVideo?"https://www.youtube.com/embed/".concat(idVideo):null)
                    console.log(datos.body.enlaceVideo)

                }

                setPropuesta(datos.body)
                console.log(idVideo)
            } else {
                await Swal.fire({
                    title: "Error",
                    text: datos.message,
                    icon: "error",
                });
            }
        }
        getData();
    }, [voto]);

    const openFinanciar = () => {
        setIsOpenFinanciar(true);
    }

    async function registrarFinanciar() {
        let response=await financiar(idPropuesta,context.usuario.id)
        if(response){
            closeFinanciar();
            openRespuesta();
        }
        else{
            closeFinanciar();
            await Swal.fire({
                title: "Error",
                text: "Ups! Ya te habías registrado previamente como patrocinador :)",
                icon: "warning",
            });
        }
    }

    const closeFinanciar = () => {
        setIsOpenFinanciar(false);
    }

    const openDonar = () => {
        setIsOpenDonar(true);
    }

    const openRespuesta = () => {
        setIsOpenRespuesta(true);
    }

    const closeRespuesta = () => {
        setIsOpenRespuesta(false);
    }

    const openRespuesta2 = () => {
        setIsOpenRespuesta2(true);
    }

    const closeRespuesta2 = () => {
        setIsOpenRespuesta2(false);
    }

    async function registrarDonar() {
        let response=await donar(idPropuesta,context.usuario.id,montoDonacion)
        if(response){
            let copy={...propuesta}
            copy.recaudacion+=parseInt(montoDonacion)
            setPropuesta(copy)
            console.log(copy)
        }
        closeDonar();
        openRespuesta();
    }

    const closeDonar = () => {
        setIsOpenDonar(false);
    }

    const openProponerContacto = () => {
        setIsOpenProponerContacto(true);
    }

    const registrarProponerContacto = () => {
        console.log("Se registro el contacto")
        closeProponerContacto();
        openRespuesta();
    }

    const closeProponerContacto = () => {
        setIsOpenProponerContacto(false);
    }
    async function vote(id) {
        const response = await votar(id);

        if (response) {
            setVoto(voto ? voto + 1 : 1)
            console.log("Se añadio 1 voto");
            openRespuesta()
        } else {
            await Swal.fire({
                title: "Error",
                text: response.message,
                icon: "error",
            });
        }
    }

    const goBack =()=>{
        console.log(props)
        if(props.location.state.idCategoria===undefined){
            props.history.push({
                pathname:"/"
            });
        }else{
            props.history.push({
                pathname: "/proyectos",
                state:{idCategoria:props.location.state.idCategoria}
            });
        }
    }

    return (
        <Grid container alignContent="stretch" className={classes.flex}>
            <Grid container spacing={2} className={classes.root}>
                <Grid item xs={12} md={7} container>
                    <Typography gutterBottom variant="h4">{propuesta?propuesta.nombre:null}</Typography>

                    { (!youtube_parser(propuesta.enlaceVideo?propuesta.enlaceVideo:""))?
                        <Grid xs={12} md={12} justify="center" align="center" justify-content="center">
                            <img src={propuesta.enlaceVideo} alt="No se subio un video aun" width="75%" />
                        </Grid>
                        :
                        <Grid xs={12} container justify="center" align="center" justify-content="center">
                            <iframe width="560" height="315" src={"https://www.youtube.com/embed/".concat(youtube_parser(propuesta.enlaceVideo?propuesta.enlaceVideo:""))} 
                                frameborder="0" 
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                                allowfullscreen>
                            </iframe>
                        </Grid>
                    }
                    <Grid xs={12}>
                        <Typography gutterBottom>{propuesta?propuesta.descripcion:null}</Typography>
                    </Grid>
                </Grid>

                
                <Grid item xs={12} s={6} md={5}>
                    {context.tipoUsuario == 'organizacion' ? null : context.tipoUsuario == null ? null :
                        <Button variant="contained" color="primary" className={classes.peruButton} onClick={() => vote(propuesta.id)}>Votar <ThumbUpIcon style={{ marginLeft: 10 }} /></Button>
                    }
                    {/*<Button variant="contained" color="primary" className={classes.peruButton} onClick={()=>goBack()}>Regresar a propuestas</Button>*/}
                    <Grid xs={12} md={8} className={classes.detailsContainer}>
                        <Grid container justify="space-between" className={classes.detailLine}>
                            <Typography>Participantes:</Typography>
                            <Typography align="right">{propuesta?propuesta.participantes:null}</Typography>
                        </Grid>

                        <Grid container justify="space-between" className={classes.detailLine}>
                            <Typography>Recaudación Esperada:</Typography>
                            <Typography align="right">S/.{propuesta?propuesta.recaudacionEsperada:null}</Typography>
                        </Grid>

                        <Grid container justify="space-between" className={classes.detailLine}>
                            <Typography>Recaudación:</Typography>
                            <Typography align="right">S/.{propuesta?propuesta.recaudacion:null}</Typography>
                        </Grid>

                        <Grid container className={classes.detailLine}>
                            <Typography>Partners:</Typography>
                            <Grid container>
                                {propuesta.lSponsors?propuesta.lSponsors.map((sponsor) =>(
                                    <Grid xs={12}>
                                        <Typography>{sponsor.razonSocial}</Typography>
                                    </Grid>
                                )):<Typography align="right">Aún sin partners. Sé el primero!</Typography>}
                            </Grid>
                        </Grid>

                        <Grid container justify="space-between" className={classes.detailLine}>
                            <Typography>Votos:</Typography>
                            <Typography align="right">{propuesta?propuesta.numVotos:null}</Typography>
                        </Grid>
                    </Grid>

                    <Grid container>
                        {context.tipoUsuario == 'organizacion' ? null : context.tipoUsuario == null ? null :
                            <div>
                                <Button variant="contained" color="primary" className={classes.peruButton} onClick={openRespuesta2}>Participar</Button>
                                <Dialog open={isOpenRespuesta2} close={closeRespuesta2}>
                                    <DialogTitle>Gracias!</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText>
                                            Gracias por tu interés en participar! En unos momentos, nos estaremos comunicando contigo por correo electrónico.
                                        </DialogContentText>
                                        <DialogActions>
                                            <Button onClick={closeRespuesta2} color="primary">
                                                Cerrar
                                            </Button>
                                        </DialogActions>
                                    </DialogContent>
                                    
                                </Dialog>
                            </div>
                        }
                        {context.tipoUsuario == 'persona' ?
                            <div>
                                <Button variant="contained" color="primary" className={classes.peruButton} onClick={openDonar}>Donar</Button>
                                <Dialog open={isOpenDonar} onClose={closeDonar}>
                                    <DialogTitle id="form-dialog-title">Realizar Donación</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText>
                                            Registre su proyecto en este formulario. Puede agregar un video promocionando el proyecto.
                                        </DialogContentText>
                                        <TextField
                                            autoFocus
                                            margin="dense"
                                            id="name"
                                            label="Monto Donación (S/.)"
                                            fullWidth
                                            onChange={ (e) => {
                                                setMontoDonacion(e.target.value)
                                            }}
                                        />
                                    </DialogContent>

                                    <DialogActions>
                                        <Button onClick={closeDonar} color="primary">
                                            Cancelar
                                        </Button>
                                        <Button onClick={registrarDonar} color="primary">
                                            Registrar
                                        </Button>
                                    </DialogActions>
                                </Dialog>
                            </div>
                        :context.tipoUsuario == 'organizacion' ?
                            <div>
                                <Button variant="contained" color="primary" className={classes.peruButton} onClick={openFinanciar}>Patrocinar</Button>
                                <Dialog open={isOpenFinanciar} onClose={closeFinanciar}>
                                    <DialogTitle id="form-dialog-title">Agregar Financiamiento</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText>
                                            Registre su proyecto en este formulario. Puede agregar un video promocionando el proyecto.
                                        </DialogContentText>
                                        <TextField
                                            autoFocus
                                            margin="dense"
                                            id="name"
                                            label="Monto Financiamiento (S/.)"
                                            fullWidth
                                        />
                                    </DialogContent>

                                    <DialogActions>
                                        <Button onClick={closeFinanciar} color="primary">
                                            Cancelar
                                        </Button>
                                        <Button onClick={registrarFinanciar} color="primary">
                                            Registrar
                                        </Button>
                                    </DialogActions>
                                </Dialog>
                            </div>
                        : null
                        }
                        {context.tipoUsuario == 'persona' ? null : context.tipoUsuario == null ? null :
                            <div>
                                <Button variant="contained" color="primary" className={classes.peruButton} onClick={openProponerContacto}>Proponer contacto</Button>
                                <Dialog open={isOpenProponerContacto} onClose={closeProponerContacto}>
                                    <DialogTitle id="form-dialog-title">Proponer contacto</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText>
                                            Te agradeceremos cualquier información que nos puedas brindar para llevar el proyecto a cabo. Déjanos la información de contacto aquí.
                                        </DialogContentText>
                                        <TextField
                                            autoFocus
                                            margin="dense"
                                            id="name"
                                            label="Correo electrónico/número telefónico"
                                            fullWidth
                                            
                                        />
                                    </DialogContent>

                                    <DialogActions>
                                        <Button onClick={closeProponerContacto} color="primary">
                                            Cancelar
                                        </Button>
                                        <Button onClick={registrarProponerContacto} color="primary">
                                            Registrar
                                        </Button>
                                    </DialogActions>
                                </Dialog>
                            </div>
                        }

                        <div>
                            <Dialog open={isOpenRespuesta} onClose={closeRespuesta}>
                                <DialogTitle>Gracias!</DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        Gracias a tu aporte, este proyecto podrá salir adelante. Mil gracias!
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={closeRespuesta} color="primary">
                                        Cerrar
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </div>
                    </Grid>
                </Grid>

            </Grid>
        </Grid>
    )
}
