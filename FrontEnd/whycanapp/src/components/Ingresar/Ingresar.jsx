import React,{ useContext,useEffect } from 'react'
import {
    Grid, Card, CardActionArea,
    CardContent, CardMedia, Typography, requirePropFactory
} from '@material-ui/core'
import { useStyles } from './Ingresar.modules'

import fondoPersonas from '../../assets/img/Personas.png'
import fondoEmpresas from '../../assets/img/Empresa.png'
import {MainContext} from '../../context/context'
import { getPersonUser, getCompanyUser } from '../../api/services'
import Swal from 'sweetalert2'

export default function Ingresar(props) {
    const context = useContext(MainContext)
    const classes = useStyles();
      
    async function logAsPerson(){
        const response=await getPersonUser();
        console.log("AVER: ",response);
        
        if (response) {
            await context.setTipoUsuario('persona')
            await context.setUsuario(response.body)
            console.log(context.usuario);
            props.history.push("/categorias")
          } else {
             await Swal.fire({
              title: "Error",
              text: response.message,
              icon: "error",
            });
          }
    }

    async function logAsCompany(){
        const response=await getCompanyUser();
        console.log("AVER: ",response);
        
        if (response) {
            await context.setTipoUsuario('organizacion')
            await context.setUsuario(response.body)
            console.log(context.usuario);
            props.history.push("/categorias")
          } else {
             await Swal.fire({
              title: "Error",
              text: response.message,
              icon: "error",
            });
          }
    }

    return (
        <div>
            <Grid container direction="row" className={classes.root}>
                <Grid item xs={12} md={6}>
                    <Card className={classes.root}>
                        <CardActionArea onClick={logAsPerson} className={classes.root}>
                            <CardMedia
                                component="img"
                                alt="fondo Personas"
                                image={fondoPersonas}
                                title="Fondo Personas"
                            />
                            
                        </CardActionArea>
                    </Card>
                </Grid>

                <Grid item xs={12} md={6}>
                    <Card>
                        <CardActionArea className={classes.root} onClick={logAsCompany}>
                            <CardMedia
                                component="img"
                                alt="fondo Empresas"
                                image={fondoEmpresas}
                                title="Fondo Empresas"
                            />
                            
                        </CardActionArea>
                    </Card>
                </Grid>
            </Grid>
        </div>
    )
}
