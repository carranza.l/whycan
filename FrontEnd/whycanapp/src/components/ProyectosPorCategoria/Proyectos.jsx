import React, { useEffect, useState, useContext } from 'react'
import { problemList, proyectByCategoryList, votar,projectRegister,proyectByProblematicList } from '../../api/services';
import {
    Grid, Card, CardActionArea,
    CardContent, CardMedia, Typography, Button, CardActions,
    TextField, Tooltip, InputAdornment, IconButton,Dialog, DialogTitle,
    DialogContent, DialogContentText, DialogActions,
} from '@material-ui/core'
import Swal from 'sweetalert2'
import { useStyles } from './Proyectos.module'
import fondoPersonas from '../../assets/img/imagen1.jpg'
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import SearchIcon from '@material-ui/icons/Search'
import { MainContext } from '../../context/context';
import FilterListIcon from '@material-ui/icons/FilterList';
import ModalProblematicas from './ModalProblematicas';

export default function Proyectos(props) {
    const idCategoria = props.location.state.idCategoria;
    const [problemas, setProblemas] = useState([]);
    const [problema,setProblema]=useState(null);
    const [proyectos, setProyectos] = useState([]);
    const [voto, setVoto] = useState("");
    const context = useContext(MainContext);
    const classes = useStyles();
    const[refresh,setRefresh]=useState(null);
    const [open, setOpen] = React.useState(false);
    const [solucion, setSolucion] = useState([]);

    const abrir = () => {
        setOpen(true);
    };

    const cerrar = () => {
        setOpen(false);
    };


    useEffect(() => {
        async function getData() {
            const datos = await problemList(idCategoria);
            if (datos.body) {
                console.log(datos);

                setProblemas(datos.body)
            } else {
                await Swal.fire({
                    title: "Error",
                    text: datos.message,
                    icon: "error",
                });
            }
        }
        getData();
    }, [])
    useEffect(() => {
        if(context.selectedProblematic){
            async function getData() {
                const datos = await proyectByProblematicList(context.selectedProblematic.id);
                if (datos) {
                    console.log("ABER", datos);
                    
                    /* datos.body.map((x) => {
                        if (x.id === idCategoria) {
                            setProyectos(x.lProyectos)
                        }
                    }) */
                    setProyectos(datos.body)
                } else {
                    await Swal.fire({
                        title: "Error",
                        text: datos.message,
                        icon: "error",
                    });
                }
            }
            getData();
        }
    }, [context.selectedProblematic])

    useEffect(() => {
        async function getData() {
            const datos = await proyectByCategoryList(idCategoria);
            if (datos) {
                console.log("ABER", datos);
                
                /* datos.body.map((x) => {
                    if (x.id === idCategoria) {
                        setProyectos(x.lProyectos)
                    }
                }) */
                setProyectos(datos.body)
            } else {
                await Swal.fire({
                    title: "Error",
                    text: datos.message,
                    icon: "error",
                });
            }
        }
        getData();
    }, [voto,refresh])


    const goToProyect = (id) => {
        props.history.push({
            pathname: "/proyecto",
            state: {
                idPropuesta: id,
                idCategoria: idCategoria
            },
        });
    }

    async function vote(id) {
        const response = await votar(id);
        console.log("AVER: ", response);

        if (response) {
            setVoto(voto ? voto + 1 : 1)
            console.log("Se añadio 1 voto");

        } else {
            await Swal.fire({
                title: "Error",
                text: response.message,
                icon: "error",
            });
        }
    }

    const registrarProyecto=()=>{
        if(context.usuario===null){
            Swal.fire({
                title: "Error",
                text: "Debe loguearse antes de proponer una Solución",
                icon: "error",
            });
        }else if(context.selectedProblematic===null){
            Swal.fire({
                title: "Error",
                text: "Por favor, seleccione una problemática",
                icon: "warning",
            });
        }else{
            setOpen(true);
        }
    }

    async function registrarSolucion(idProblematica, idPersona, nombre, descripcion, urlVideo,recaudacionEsperada) {
        const datos = await projectRegister(idProblematica, idPersona, nombre, descripcion, urlVideo,recaudacionEsperada);
        if (datos.body) {
            setRefresh(refresh?refresh+1:1)
            await Swal.fire({
                title: "Exito",
                text: "Se registró la propuesta",
                icon: "success",
            });
            //console.log(datos)
        } else {
            await Swal.fire({
                title: "Error",
                text: datos.message,
                icon: "error",
            });
        }
    }

    const registrar = () => {
        if(solucion.videoLink===null){
            solucion.videoLink=""
        }
        registrarSolucion(context.selectedProblematic.id, context.usuario.id, solucion.titulo, solucion.descripcion, solucion.videoLink,solucion.recaudacionEsperada);
        setOpen(false);
    };


    const openModal = () => {
        
        context.setOpenProbModal(true);
        
    }

    return (
        <div>

            <Grid item container direction="row" spacing={2} justify="space-evenly" alignItems="flex-start">
                <Grid item style={{ marginLeft: 20 }} xs={12} sm={4}>
                    <h1>Seleccione un Proyecto</h1>

                </Grid>
                <Grid item xs={7} sm={4} style={{marginLeft:4}}>
                    <TextField
                        disabled={context.selectedProblematic === null}
                        value={context.selectedProblematic ? context.selectedProblematic.nombre : null}
                        fullWidth
                        id="input-with-icon-textfield"
                        label="Visualizar por Problematica"
                        InputLabelProps={{ shrink: context.selectedProblematic !== null, }}
                        InputProps={{
                            endAdornment: (
                                <Tooltip title="Filtrar por Problemática">
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="Filtrar por Problemática"
                                            onClick={() => openModal()}
                                        >
                                            <FilterListIcon />
                                        </IconButton>
                                    </InputAdornment>
                                </Tooltip>
                            ),
                        }}
                    />
                    <ModalProblematicas id={idCategoria} />
                </Grid>
                        {context.usuario!==null && context.usuario.razonSocial===undefined&&
                <Grid item xs={4} sm={3}>
                    <Grid item  container direction="column" alignContent="center" alignItems="center" justify="center" style={{marginTop:20}}>
                        <Grid item>
                            <label>¿Tienes una propuesta?</label>
                        </Grid>
                        <Grid item style={{marginBottom:5}}>
                            <label>¡Regístrala aquí!</label> 
                        </Grid>
                        <Grid item>
                        <Button className={classes.peruButton} onClick={()=>registrarProyecto()}>Registrar Idea</Button>
                        </Grid>
                    </Grid>
                </Grid>
                        
                        }
            </Grid>
            <Grid spacing={5} container alignContent="stretch" justify="center" className={classes.flex}>
                {proyectos ? proyectos.map((x) => (
                    <Grid item xs={12} sm={6} md={3} key={x.id}>
                        <Card className={classes.flex} >
                            <CardActionArea onClick={() => goToProyect(x.id)} className={classes.flex}>
                                <CardMedia
                                    height="100%"
                                    component="img"
                                    alt="fondo Personas"
                                    image={fondoPersonas}
                                    title={x.nombre}
                                />
                                <CardContent style={{height:150}}>
                                    <Grid container direction="column" alignItems="center" justify="flex-end">
                                        <Grid item>
                                            <Typography gutterBottom variant="h6" component="h2">
                                                {x.nombre}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </CardActionArea>
                            <CardActions disableSpacing>
                                <Grid container direction="column" alignContent="center" justify="center">
                                    <Grid item>
                                        <Typography ><span>Vote aquí: </span></Typography>
                                    </Grid>
                                    <Grid item>
                                        <Button size="large" onClick={() => vote(x.id)}>{x.numVotos}<ThumbUpIcon style={{ marginLeft: 10 }} /></Button>
                                    </Grid>
                                </Grid>
                            </CardActions>
                        </Card>
                    </Grid>
                )) : null}
            </Grid>
            <Dialog open={open} onClose={cerrar}>
                <DialogTitle id="form-dialog-title">Registrar Proyecto</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Registre su proyecto en este formulario. Puede agregar un video promocionando el proyecto.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Título del proyecto"
                        fullWidth
                        onChange={(e) => {
                            let copy = {...solucion};
                            copy.titulo=e.target.value;
                            setSolucion(copy);
                        }}
                    />
                    <TextField
                        margin="dense"
                        id="name"
                        label="Descripción del proyecto"
                        rows={4}
                        fullWidth
                        multiline
                        onChange={(e) => {
                            let copy = {...solucion};
                            copy.descripcion=e.target.value;
                            setSolucion(copy);
                        }}
                    />
                    <TextField
                        margin="dense"
                        id="name"
                        label="Recaudación Esperada"
                        fullWidth
                        onChange={(e) => {
                            let copy = {...solucion};
                            copy.recaudacionEsperada=e.target.value;
                            setSolucion(copy);
                        }}
                    />
                    <TextField
                        margin="dense"
                        id="name"
                        label="Link de video"
                        fullWidth
                        onChange={(e) => {
                            let copy = {...solucion};
                            copy.videoLink=e.target.value;
                            setSolucion(copy);
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={cerrar} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={registrar} color="primary">
                        Registrar
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}
