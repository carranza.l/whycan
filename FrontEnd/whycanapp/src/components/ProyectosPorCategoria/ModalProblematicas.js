import React, { useContext, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Dialog, Grid, Button, DialogTitle, DialogContent, DialogActions,Card,
  CardMedia,
  CardContent,
  CardActions,
  CardActionArea,Typography } from '@material-ui/core';
import { useStyles } from './ModalProblematicas.module'
import { problemList } from '../../api/services';
import Carousel from 'react-multi-carousel';
import {
  isBrowser,
  isMobile,
  isTablet
} from "react-device-detect";
import Swal from "sweetalert2";
import { MainContext } from '../../context/context';

const responsive = {
  desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 2,
      slidesToSlide: 2 // optional, default to 1.
  },
  tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
      slidesToSlide: 1 // optional, default to 1.
  },
  mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1 // optional, default to 1.
  }
};

export default function ModalProblematicas(props) {
    const classes = useStyles();
    const context = useContext(MainContext)
    const [problemLista, setProblemList] = useState();
    const [problematic,setProblematic] = useState();

  React.useEffect(() => {
    async function getData() {
      const datos = await problemList(props.id);
      if (datos) {
        setProblemList(datos.body);
        console.log(datos.body);
      } else {
        await Swal.fire({
          title: "Error",
          text: datos.message,
          icon: "error",
        });
      }
    }
    getData();
  }, []);

  const selectProblematic=(problematic)=>{
    context.setSelectedProblematic(problematic);
    console.log(problematic);
    context.setOpenProbModal(false);
  }


  const handleClose = () => {
    context.setOpenProbModal(false);
  };

  return (
    <Dialog
      modal={true}
      autoDetectWindowHeight={false}
      autoScrollBodyContent={false}
      contentStyle={{ width: "200%", maxWidth: "0" }}
      open={context.openProbModal}
      onClose={() => handleClose()}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      disableBackdropClick={true}
    >
      <DialogTitle onClose={() => { handleClose() }}>
        Seleccionar Problemática
    </DialogTitle>
      <DialogContent>
        <Card>
      <Grid container alignContent="stretch" justify="center" className={classes.flex}>
                        <Grid item xs={12} alignContent="stretch" justify="center" className={classes.flex}>
                            <Carousel
                                swipeable={true}
                                draggable={false}
                                showDots={false}
                                responsive={responsive}
                                ssr={true} // means to render carousel on server-side.
                                infinite={true}
                                autoPlay={isMobile ? true : false}
                                autoPlaySpeed={999999}
                                keyBoardControl={false}
                                transitionDuration={500}
                                containerClass="carousel-container"
                                removeArrowOnDeviceType={["tablet", "mobile"]}
                                deviceType={isMobile ? "mobile" : isTablet ? "tablet" : "desktop"}
                                dotListClass="custom-dot-list-style"
                                itemClass="carousel-item-padding-40-px"
                                >
                                {problemLista?problemLista.map((x) => (
                                  <Card className={classes.flex} >
                                        <CardActionArea  className={classes.flex} onClick={() => selectProblematic(x)}>
                                            <CardMedia
                                            height="400"
                                            
                                            component="img"
                                            alt=""
                                            image={x.urlImagen}
                                            title={x.nombre} 
                                            />
                                            <CardContent style={{ height: 150 }}>
                                                <Typography gutterBottom variant="h6" component="h3" >
                                                    {x.nombre}
                                                </Typography>
                                                
                                            </CardContent>
                                        </CardActionArea>
                                    </Card>
                                )):null}
                            </Carousel>
                        </Grid>
                    </Grid>
        </Card>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => { handleClose() }}>Cerrar</Button>
      </DialogActions>

    </Dialog>
  );
}
