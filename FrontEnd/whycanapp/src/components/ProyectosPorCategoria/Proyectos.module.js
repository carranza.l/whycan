import React from 'react'
import { makeStyles } from '@material-ui/core'

export const useStyles=makeStyles((theme)=>({
    root: {
        width:"100%",
        height:"100%",
        maxWidth: "100%",
        maxHeight:"100%"
    },
    peruButton:{
        marginLeft: "10px",
        color: 'white',
        backgroundColor: '#9a031e',
        '&:hover':{
          color: 'black',
          backgroundColor: 'white'
        }      
    },
}))
