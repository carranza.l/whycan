import React, { useContext } from 'react';
import {
    AppBar,
    Toolbar,
    Typography,
    CssBaseline,
    Link
} from "@material-ui/core";
import logo from "../../assets/img/logoKawsay.png";
import { useStyles } from "./Appbar.module";
import { MainContext } from '../../context/context';

export default function Appbar(props) {
    const classes = useStyles();
    const context = useContext(MainContext);

    const goToAccess = () => {
        props.history.push({
            pathname: "/ingresar",
        })
    }

    const goToRegister = () => {
        props.history.push({
            pathname: "/register",
        })
    }

    return (
        <div>
            <CssBaseline />
            <AppBar position="static" color="default" elevation={0} className={classes.appBar}>
                <Toolbar className={classes.toolbar}>
                    <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
                        <Link href="/">
                            <img src={logo} alt="Logo" height="50" />
                        </Link>
                    </Typography>
                    <nav>
                        {context.usuario === null &&
                            <div>
                                <Link onClick={goToAccess} variant="button" href="/ingresar" className={classes.link}>
                                    Ingresar
                                </Link>
                                {/*<Link onClick={goToRegister} variant="button" href="/registrar" className={classes.link}>
                                    Registrarse
                                </Link>*/}
                            </div>
                        }{context.usuario !== null &&
                            <div>
                                <div className={classes.link}>
                                    Bienvenido {context.usuario ? context.usuario.nombreUsuario : null}
                                </div>
                            </div>
                        }
                    </nav>
                </Toolbar>
            </AppBar>
        </div>
    )
}
