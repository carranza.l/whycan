import React,{ useEffect, useState } from 'react'
import { problemList } from '../../api/services';
import { Grid, Card, CardActionArea,
    CardContent, CardMedia, Typography } from '@material-ui/core'
import Swal from 'sweetalert2'
import { useStyles } from './Problematicas.module'
import fondoPersonas from '../../assets/img/Personas.png'

export default function Problematicas(props) {
    const idCategoria=props.location.state.idCategoria;
    console.log(props);
    const [problemas, setProblemas]=useState()
    const classes=useStyles();

    useEffect(()=>{
        async function getData() {
          const datos = await problemList(idCategoria);
          if (datos) {
              console.log(datos);
              
            setProblemas(datos.body)
          } else {
            await Swal.fire({
              title: "Error",
              text: datos.message,
              icon: "error",
            });
          }
        }
        getData();
      },[])

    const goToProblem =(id)=>{
        props.history.push({
            pathname:"/problematica",
            state:{idProblematica:id},
        });
    }

    return (
        <div>
        <Grid container direction="column">
                <Grid item style={{marginLeft:50,marginTop:10}}>
                    <h1>Seleccione una Problemática</h1>
                    <br/>

                </Grid>
            </Grid>
        <Grid spacing={5} container alignContent="stretch" justify="center" className={classes.flex} style={{marginTop:30}}>
            {problemas?problemas.map((x) => (
                <Grid item xs={12} sm ={6} md={3} key={x.id}>
                    <Card className={classes.flex} >
                        <CardActionArea onClick={()=>goToProblem(x.id)} className={classes.flex}>
                            <CardMedia
                                component="img"
                                alt="fondo Personas"
                                image={fondoPersonas}
                                title={x.nombre}
                            />
                            <CardContent>
                                <Grid container direction="column" alignItems="center" justify="flex-end">
                                    <Grid item>
                                        <Typography gutterBottom variant="h6" component="h2">
                                            {x.nombre}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>
            )):null}
        </Grid>
        </div>
    )
}
