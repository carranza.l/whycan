import React from 'react'
import { makeStyles } from '@material-ui/core'

export const useStyles=makeStyles((theme)=>({
    root: {
        width:"100%",
        height:"100%",
        maxWidth: "100%",
        maxHeight:"100%"
    },
}))
