import React, { useEffect, useState } from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import {
    isBrowser,
    isMobile,
    isTablet
} from "react-device-detect";
import {
    Toolbar,
    Typography,
    CssBaseline,
    Container,
    Grid,
    Button,
    Card,
    CardMedia,
    CardContent,
    CardActions,
    CardActionArea,
    Link
} from "@material-ui/core";
import { useStyles } from "./Home.module";
import imagen1 from "../../assets/img/imagen1.jpg";
import imagen2 from "../../assets/img/imagen2.jpg";
import { proyectListTop, votar } from '../../api/services';
import Swal from 'sweetalert2';

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const sections = [
    { title: 'Ver Categorias', url: '/categorias' }
    /*{ title: 'Business', url: '#' },
    { title: 'Politics', url: '#' },
    { title: 'Opinion', url: '#' },
    { title: 'Science', url: '#' },
    { title: 'Health', url: '#' },
    { title: 'Style', url: '#' },
    { title: 'Travel', url: '#' },*/
];
const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 4,
        slidesToSlide: 2 // optional, default to 1.
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        slidesToSlide: 1 // optional, default to 1.
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1 // optional, default to 1.
    }
};

export default function Home(props) {
    const classes = useStyles();
    const [proyectosTop, setProyectosTop] = useState([]);
    const [voto, setVoto] = useState("");

    useEffect(() => {
        async function getData() {
            const datos = await proyectListTop();
            if (datos) {
                setProyectosTop(datos.body)
                console.log(datos.body);
                
            } else {
                await Swal.fire({
                    title: "Error",
                    text: datos.message,
                    icon: "error",
                });
            }
        }
        getData();
    }, [voto]);
    const goToProject = (id) => {
        props.history.push({
            pathname: "/proyecto",
            state: { idPropuesta: id },
        });
    };

    async function vote(id) {
        const response = await votar(id);
        console.log("AVER: ", response);

        if (response) {
            setVoto(voto ? voto + 1 : 1)
            console.log("Se añadio 1 voto");

        } else {
            await Swal.fire({
                title: "Error",
                text: response.message,
                icon: "error",
            });
        }
    }
    return (
        <React.Fragment>
            <CssBaseline />
            <Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>
                {sections.map((section) => (
                    <Link
                        color="inherit"
                        noWrap
                        key={section.title}
                        variant="body2"
                        href={section.url}
                        className={classes.toolbarLink}
                    >
                    </Link>
                ))}
            </Toolbar>
            <main>
                {/* Hero unit */}
                <div className={classes.heroContent} >
                    <Card>
                    <Grid container direction="row" spacing={0} alignItems="center" justify="center">
                        <Grid item xs={12} md={6} justify="center" align="center" justify-content="center">
                            <Typography component="h1" variant="h2" color="textPrimary" align="center" justify-content="center">
                                Sé parte de una red colaborativa
                </Typography>
                        </Grid>
                        <Grid item xs={12} md={6} justify="center" align="center" justify-content="center">
                            <img src={imagen1} alt="imagen 1" width="100%" />
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={0} alignItems="center" justify="center">
                        <Grid item xs={12} md={6} justify="center" align="center" justify-content="center">
                            <img src={imagen2} alt="imagen 2" width="100%" />
                        </Grid>
                        <Grid item xs={12} md={6} justify="center">
                            <Grid container direction="column" spacing={3}>
                                <Grid item style={{marginLeft:20,marginRight:20}}>
                                    <Typography component="p" variant="overline" color="textPrimary" align="center" justify-content="center">
                                        Con Kawsay buscamos impulsar el contacto entre personas, ONG's y empresas para identificar problemáticas en distintas zonas y proponer soluciones para cada una. Mediante la colaboración colectiva lograremos financiar e implementar las mejores soluciones
                                    </Typography>
                                </Grid>
                                <Grid item align="center" justify-content="center">
                                    <Button className={classes.peruButton} href='/categorias' style={{width:180,height:50}}>
                                        Descubre más
                  </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    </Card>

                    <br />
                            <Card>

                    <Grid container direction="row" spacing={0}>
                        <Grid item xs={12}>
                            <Typography component="h5" gutterBottom paragraph variant="h4" color="textPrimary" style={{marginLeft:20,marginTop:20}}>
                                <b>Propuestas más populares</b>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container alignContent="stretch" justify="center" className={classes.flex}>
                        <Grid item xs={12} alignContent="stretch" justify="center" className={classes.flex}>
                            <Carousel
                                swipeable={true}
                                draggable={false}
                                showDots={false}
                                responsive={responsive}
                                ssr={true} // means to render carousel on server-side.
                                infinite={false}
                                autoPlay={isMobile ? true : false}
                                autoPlaySpeed={999999}
                                keyBoardControl={false}
                                transitionDuration={500}
                                containerClass="carousel-container"
                                removeArrowOnDeviceType={["tablet", "mobile"]}
                                deviceType={isMobile ? "mobile" : isTablet ? "tablet" : "desktop"}
                                dotListClass="custom-dot-list-style"
                                itemClass="carousel-item-padding-40-px"
                                >
                                {proyectosTop.map((x) => (
                                    <Card className={classes.flex} >
                                        <CardActionArea  className={classes.flex}>
                                            <CardMedia
                                            height="400"
                                            onClick={() => goToProject(x.id)}
                                            component="img"
                                            alt=""
                                            image={x.Categoria.urlImagen}
                                            title={x.nombre}
                                            />
                                            <CardContent style={{ height: 150 }}>
                                                <Typography gutterBottom variant="h6" component="h3" >
                                                    {x.nombre}
                                                </Typography>
                                                
                                            </CardContent>
                                        <CardActions disableSpacing>
                                            <Grid container direction="column" alignContent="center" justify="center">
                                                <Grid item>
                                                    <Typography ><span>Vote aquí: </span></Typography>
                                                </Grid>
                                                <Grid item>
                                                    <Button size="large" onClick={() => vote(x.id)}>{x.numVotos}<ThumbUpIcon style={{ marginLeft: 10 }} /></Button>
                                                </Grid>
                                            </Grid>
                                        </CardActions>
                                        </CardActionArea>
                                    </Card>
                                ))}
                            </Carousel>
                        </Grid>
                    </Grid>
                                </Card>
                </div>
            </main>
        </React.Fragment>
    );
}