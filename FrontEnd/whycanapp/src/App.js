import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Appbar from './components/Appbar/Appbar'
import Home from './components/Home/Home'
import Ingresar from './components/Ingresar/Ingresar';
import Categorias from './components/Categorias/Categorias';
import Problematica from './components/Problematica/Problematica';
import Problematicas from './components/Problematicas/Problematicas';
import Proyectos from './components/ProyectosPorCategoria/Proyectos';
import Proyecto from './components/Proyecto/Proyecto';
import CrearSolucion from './components/CrearSolucion/CrearSolucion';

function App() {
  return (
    <div>
    <Router>
      <Appbar />
      <Switch>
        <Route exact path ="/" component={Home}/>
        <Route exact path ="/ingresar" component={Ingresar}/>
        <Route exact path ="/categorias" component={Categorias}/>
        <Route exact path ="/problematica" component={Problematica}/>
        <Route exact path ="/problematicas" component={Problematicas}/>
        <Route exact path ="/proyectos" component={Proyectos}/>
        <Route exact path ="/proyecto" component={Proyecto}/>
        <Route exact path ="/crearsolucion" component={CrearSolucion}/>
      </Switch>
    </Router>
    </div>
  );
}

export default App;
