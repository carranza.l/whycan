import Ingresar from "./components/Ingresar/Ingresar";
import Home from "./components/Home/Home";
import Problematicas from "./components/Problematicas/Problematicas";
import Proyectos from "./components/ProyectosPorCategoria/Proyectos";
import Proyecto from "./components/Proyecto/Proyecto";
import CrearSolucion from "./components/CrearSolucion/CrearSolucion";

export const routes = [
    {
        path: "/ingreso",
        name: "Ingreso",
        layout: "/",
        component: Ingresar,
    },
    {
        path: "/inicio",
        name: "Inicio",
        layout: "/",
        component: Home,
    },
    {
        path: "/categorias",
        name: "Categorias",
        layout: "/",
        component: Categorias,
    },
    {
        path: "/problematica",
        name: "Problematica",
        layout: "/",
        component: Problematica,
    },
    {
        path: "/problematicas",
        name: "Problematicas",
        layout: "/",
        component: Problematicas,
    },
    {
        path: "/proyectos",
        name: "Proyectos",
        layout: "/",
        component: Proyectos,
    },
    {
        path: "/proyecto",
        name: "Proyecto",
        layout: "/",
        component: Proyecto,
    },
    {
        path: "crearsolucion",
        name: "crearsolucion",
        layout: "/",
        component: CrearSolucion
    }
]