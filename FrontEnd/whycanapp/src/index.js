import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { MainProvider } from './context/context';

ReactDOM.render(
  <MainProvider>
      <App />
  </MainProvider> ,
  document.getElementById('root')
);
