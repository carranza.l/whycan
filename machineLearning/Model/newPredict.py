import _pickle as cPickle
import re
import pandas as pd
import boto3
import subprocess
from collections import Counter
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
import requests

def clean_tokens(text):
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens

def envioABack():
    problematica ={
        "nombre" : "Robos al paso en Puno",
        "descripcion" : " - ",
        "urlImagen" : "https://e.rpp-noticias.io/normal/2020/05/28/344034_948900.png",
        "fecha" : "2020-05-28T15:32:13-05:00",
        "idDepartamento":"1",
        "idCategoria":"1"
    }
    url = 'http://ec2-54-209-216-51.compute-1.amazonaws.com:5000/api/problematica/addOne'
    x = requests.post(url, json = problematica)
    print(x.text)

def limpieza(df):
    aEliminar = r'[^A-Za-zñáéíóú]'
    for i in range(len(df)):
        df.nombre[i]=re.sub(aEliminar,' ',df.nombre[i])
        df.descripcion[i]=re.sub(aEliminar,' ',df.descripcion[i])
        
    for i in range(len(df)):
        df.nombre[i]=df.nombre[i].lower()
        df.descripcion[i]=df.descripcion[i].lower()
    
    allNews=""
    for i in range(len(df)):
        allNews+=df.descripcion[i]

    tokensTotales = allNews.split()
    
    noticias = []
    for i in range(len(df)):
        noticias.append(df.descripcion[i])    
    return noticias

def generaStopwords():	
	nltk.download('stopwords')
	nltk.download('punkt')
	stopwords_esp = nltk.corpus.stopwords.words('spanish')
	porter = nltk.PorterStemmer()
	return stopwords_esp


def table():
    session = boto3.Session(
    aws_access_key_id="AKIASSAE7W3RSCE4YHU5",
    aws_secret_access_key="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",
    )
    dynamodb = session.resource('dynamodb',region_name='us-east-1')
    table = dynamodb.Table('noticiasRPP')
    return table

def tableTag():
    session = boto3.Session(
    aws_access_key_id="AKIASSAE7W3RSCE4YHU5",
    aws_secret_access_key="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",
    )
    dynamodb = session.resource('dynamodb',region_name='us-east-1')
    table = dynamodb.Table('noticiasConCategoria')
    return table

def mainPredict():
    tableRPP=table()
    x=tableRPP.scan()
	
    dfDynamo=pd.DataFrame(x.items())
    dfNoticias=pd.DataFrame(dfDynamo[1][0])
    dfPreproc=dfNoticias.copy()

    noticias=limpieza(dfPreproc)
    stopwords=generaStopwords()

    x_test = TfidfVectorizer(tokenizer=clean_tokens, stop_words=stopwords).fit_transform(noticias)

    with open('linearsvcmodel.pkl', 'rb') as file:
        linearsvcmodel=cPickle.load(file)


    print(x_test)
    y_test=linearsvcmodel.predict(x_test)

    dfNoticias.idCategoria = y_test

    print(dfNoticias)
    awsTagtable = tableTag()
    for problematica in xTag:
        try:          
            noticia=ObtenerNoticia(urlNoticias, departamento)
            awsTagtable.put_item(Item=problematica,ConditionExpression='attribute_not_exists(nombre)')
            output.append(noticia)
        except:
            print(sys.exc_info()[0])


def lambda_handler(event, context):
    # TODO implement
    mainPredict()

if __name__ == "__main__":
    lambda_handler(None, None)