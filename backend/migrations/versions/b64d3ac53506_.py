"""empty message

Revision ID: b64d3ac53506
Revises: 9faa60a31601
Create Date: 2020-05-30 17:29:25.830472

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b64d3ac53506'
down_revision = '9faa60a31601'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('categoria', sa.Column('urlImagen', sa.String(length=120), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('categoria', 'urlImagen')
    # ### end Alembic commands ###
