"""empty message

Revision ID: 9faa60a31601
Revises: 087c86a844af
Create Date: 2020-05-30 14:56:53.647830

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9faa60a31601'
down_revision = '087c86a844af'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('organizacion', sa.Column('razonSocial', sa.String(length=150), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('organizacion', 'razonSocial')
    # ### end Alembic commands ###
