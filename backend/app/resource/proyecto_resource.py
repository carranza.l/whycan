from . import Rpta
from flask_restful import Resource
from flask import Flask, request
from app.controller.proyecto_controller import ProyectoController

class ProyectoAddOne(Resource):

    def post(self):
        try:
            data = request.get_json()
            id_problematica =  data['idProblematica']
            id_persona = data['idPersona']
            nombre = data['nombre']
            descripcion = data['descripcion']
            url_video = data['urlVideo']
            recaudacion_esperada = data['recaudacionEsperada']
            ans = ProyectoController.add_one(id_problematica,id_persona,nombre,descripcion,url_video,recaudacion_esperada)
            return ans.toJson()
        except Exception as e:
            ans = Rpta()
            ans.setError('No se agrego correctamente ','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProyectoAddVote(Resource):

    def post(self):
        try:
            data = request.get_json()
            id_proyecto = data['idProyecto']
            ans = ProyectoController.add_vote(id_proyecto)
            return ans.toJson()
        except Exception as  e:
            ans = Rpta()
            ans.setError('No se agrego un voto correctamente ','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProyectoAddDonacio(Resource):

    def post(self):
        try:
            data = request.get_json()
            id_proyecto = data['idProyecto']
            id_persona = data['idPersona']
            donacion = data['donacion']
            ans = ProyectoController.add_donacion(id_proyecto,id_persona,donacion)
            return ans.toJson()
        except Exception as  e:
            ans = Rpta()
            ans.setError('No se agrego la donacion correctamente ','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProyectoAddParticipacion(Resource):

    def post(self):
        try:
            data = request.get_json()
            id_proyecto = data['idProyecto']
            id_persona = data['idPersona']
            ans = ProyectoController.add_partipacion(id_proyecto,id_persona)
            return ans.toJson()
        except Exception as  e:
            ans = Rpta()
            ans.setError('No se agrego un voto correctamente ','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProyectoGetMejoresProyectoByCategorias(Resource):

    def post(self):
        try:
            data = request.get_json()
            id_categoria = data['idCategoria'] 
            ans = ProyectoController.get_mejores_proyecto_por_categoria(id_categoria)
            return ans.toJson()
        except Exception as  e:
            ans = Rpta()
            ans.setError('No se listo correctamente ','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProyectoGetMejoresProyectoByProblematica(Resource):

    def post(self):
        try:
            data = request.get_json()
            id_problematica = data['idProblematica'] 
            ans = ProyectoController.get_mejores_proyecto_por_problematica(id_problematica)
            return ans.toJson()
        except Exception as  e:
            ans = Rpta()
            ans.setError('No se listo correctamente ','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProyectoGetMejoresProyectos(Resource):

    def post(self):
        try:
            ans = ProyectoController.get_mejores_proyectos()
            return ans.toJson()
        except Exception as  e:
            ans = Rpta()
            ans.setError('No se listo correctamente ','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProyectoGetOne(Resource):

    def post(self):
        try:
            data = request.get_json()
            id_proyecto = data['idProyecto']
            ans = ProyectoController.get_one(id_proyecto)
            return ans.toJson()
        except Exception as e:
            ans = Rpta()
            ans.setError('No se listo correctamente ','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProyectoAddPatrocinador(Resource):

    def post(self):
        try:
            data = request.get_json()
            id_proyecto = data['idProyecto']
            id_organizacion = data['idOrganizacion']
            ans = ProyectoController.add_patrocinador(id_proyecto,id_organizacion)
            return ans.toJson()
        except Exception as e:
            ans = Rpta()
            ans.setError('No se agrego el patrocinador ','Error : {}'.format(str(e)))
            return ans.toJson() 