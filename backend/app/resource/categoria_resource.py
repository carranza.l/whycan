from . import Rpta
from flask_restful import Resource
from flask import Flask, request
from app.controller.categoria_controller import CategoriaController

class CategoriaGetAll(Resource):

    def post(self):
        try:
            
            ans = CategoriaController.get_all()
            return ans.toJson()
        except Exception as e:
            ans = Rpta()
            ans.setError('No se listo correctamente','Error : {}'.format(str(e)))
            return ans.toJson() 