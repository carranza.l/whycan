from . import Rpta
from flask_restful import Resource
from flask import Flask, request
from app.controller.organizacion_controller import OrganizacionController

class OrganizacionResourceGetOne(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_organizacion =  data['idOrganizacion']
            ans = OrganizacionController.get_one(id_organizacion)
            return ans.toJson()
        except Exception as e:
            ans = Rpta()
            ans.setError('No se obtuvo correctamente','Error : {}'.format(str(e)))
            return ans.toJson() 
