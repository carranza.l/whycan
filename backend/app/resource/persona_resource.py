from . import Rpta
from flask_restful import Resource
from flask import Flask, request
from app.controller.persona_controller import PersonaController

class PersonaResourceGetOne(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_persona =  data['idPersona']
            ans = PersonaController.get_one(id_persona)
            return ans.toJson()
        except Exception as e:
            ans = Rpta()
            ans.setError('No se obtuvo correctamente','Error : {}'.format(str(e)))
            return ans.toJson() 