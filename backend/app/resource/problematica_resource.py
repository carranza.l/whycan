from . import Rpta
from flask_restful import Resource
from flask import Flask, request
from app.controller.problematica_controller import ProblematicaController

class ProblematicaListaProyecto(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_problematica =  data['idProblematica']
            ans = ProblematicaController.get_list_proyect(id_problematica)
            return ans.toJson()
        except Exception as e:
            ans = Rpta()
            ans.setError('No se listo correctamente','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProblematicaListaByCategoria(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_categoria =  data['idCategoria']
            ans = ProblematicaController.get_lista_problematica_by_categoria(id_categoria)
            return ans.toJson()
        except Exception as e:
            ans = Rpta()
            ans.setError('No se listo correctamente','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProblematicaGetOne(Resource):
    def post(self):
        try:
            data = request.get_json()
            id_problematica = data['idProblematica']
            ans = ProblematicaController.get_one(id_problematica)
            return ans.toJson()
        except Exception as e:
            ans = Rpta()
            ans.setError('No se listo correctamente','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProblematicaAddOne(Resource):
    def post(self):
        try:
            data = request.get_json()
            nombre = data['nombre']
            descripcion = data['descripcion']
            url_imagen = data['urlImagen']
            fecha = data['fecha']
            id_departamento = data['idDepartamento']
            id_categoria = data['idCategoria']
            ans = ProblematicaController.add_one(nombre,descripcion,url_imagen,fecha,id_departamento,id_categoria)
            return ans.toJson()
        except Exception as e:
            ans = Rpta()
            ans.setError('No se agrego correctamente','Error : {}'.format(str(e)))
            return ans.toJson() 

class ProblematicaAddList(Resource):
    def post(self):
        try:
            data = request.get_json()
            l_problematica = data['lProblematica']
            ans = ProblematicaController.add_from_list(l_problematica)
            return ans.toJson()

        except Exception as e:
            ans = Rpta()
            ans.setError('No se agrego correctamente la lista de problemas ','Error : {}'.format(str(e)))
            return ans.toJson() 