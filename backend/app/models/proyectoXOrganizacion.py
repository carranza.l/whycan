from . import db
from app.models.organizacion import Organizacion
from app.models.proyecto import Proyecto

class ProjectXOrganizacion(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    idProyecto = db.Column(db.ForeignKey(Proyecto.id))
    idOrganizacion = db.Column(db.ForeignKey(Organizacion.id))

    def to_json(self):
        return {
            'id' : self.id,
            'idProyecto' : self.idProyecto,
            'idOrganizacion' : self.idOrganizacion
        }