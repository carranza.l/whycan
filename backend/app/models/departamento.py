from . import db
from app.models.region import Region


class Departamento(db.Model):
    id =  db.Column(db.Integer, primary_key = True)
    nombre = db.Column(db.String(120))
    idRegion = db.Column(db.ForeignKey(Region.id))

    def to_json(self):
        return {
            'id' : self.id,
            'nombre' : self.nombre,
            'idRegion' : self.idRegion
        }