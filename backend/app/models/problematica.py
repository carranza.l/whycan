from . import db
from datetime import datetime
from app.models.departamento import Departamento
from app.models.categoria import Categoria

class Problematica(db.Model):
    id = db.Column(db.Integer,primary_key = True )
    nombre = db.Column(db.String(120))
    descripcion = db.Column(db.String(120))
    fecha = db.Column(db.DateTime)
    idCategoria = db.Column(db.ForeignKey(Categoria.id))
    idDepartamento = db.Column(db.ForeignKey(Departamento.id))
    urlImagen = db.Column(db.String(220))
    

    def to_json(self):
        return {
            'id': self.id,
            'nombre' : self.nombre,
            'descripcion' : self.descripcion,
            'fecha' : self.fecha.__str__(),
            'idCategoria' : self.idCategoria,
            'idDepartamento' : self.idDepartamento,
            'urlImagen' : self.urlImagen
        }
    