from . import db

class Region(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    nombre = db.Column(db.String(50))

    def to_json(self):
        return {
            'id' : self.id,
            'nombre' : self.nombre
        }