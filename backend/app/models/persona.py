from . import db

class Persona(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    nombre = db.Column(db.String(150))
    nombreUsuario = db.Column(db.String(100))
    contrasena = db.Column(db.String(100))
    email = db.Column(db.String(120))
    celular = db.Column(db.String(100))

    def to_json(self):
        return {
            'id' : self.id,
            'nombre' : self.nombre,
            'nombreUsuario' : self.nombreUsuario,
            'contrasena' : self.contrasena,
            'email' : self.email,
            'celular': self.celular
        }