from . import db
from app.models.problematica import Problematica

class Proyecto(db.Model):
    id = db.Column(db.Integer,primary_key = True)
    nombre = db.Column(db.String(120))
    estado = db.Column(db.String(120)) # REGISTRADO, PATROCINADO, REALIZADO, NO COMPLETADO, 
    descripcion = db.Column(db.String(200))
    enlaceVideo = db.Column(db.String(120))
    numVotos = db.Column(db.Integer)
    recaudacion = db.Column(db.Integer)
    participantes = db.Column(db.Integer)
    idProblematica = db.Column(db.ForeignKey(Problematica.id))
    recaudacionEsperada = db.Column(db.Float)

    def to_json(self):
        return {
            'id' : self.id,
            'nombre' : self.nombre,
            'estado' : self.estado,
            'descripcion' : self.descripcion,
            'enlaceVideo' : self.enlaceVideo,
            'numVotos' : self.numVotos,
            'recaudacion' : self.recaudacion,
            'participantes': self.participantes,
            'idProblematica': self.idProblematica,
            'recaudacionEsperada' : self.recaudacionEsperada
        }
    