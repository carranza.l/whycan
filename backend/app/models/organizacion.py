from . import db

class Organizacion(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    nombreUsuario = db.Column(db.String(50))
    contrasena = db.Column(db.String(50))
    email = db.Column(db.String(120))
    nombreContacto = db.Column(db.String(120))
    rubro = db.Column(db.String(120))
    ong = db.Column(db.Boolean)
    razonSocial = db.Column(db.String(150))

    def to_json(self):
        return {
            'id' : self.id,
            'nombreUsuario' : self.nombreUsuario,
            'contrasena' : self.contrasena,
            'email' : self.email,
            'nombreContacto' : self.nombreContacto,
            'rubro' : self.rubro,
            'ong' : self.ong,
            'razonSocial' : self.razonSocial
        }