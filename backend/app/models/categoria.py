from . import db

class Categoria(db.Model):
    id = db.Column(db.Integer,primary_key = True)
    nombre = db.Column(db.String(60))
    descripcion = db.Column(db.String(120))
    urlImagen = db.Column(db.String(120))

    def to_json(self):
        return {
            'id' : self.id,
            'nombre' : self.nombre,
            'descripcion' : self.descripcion,
            'urlImagen' : self.urlImagen
        }