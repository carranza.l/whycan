from . import db
from app.models.proyecto import Proyecto
from app.models.persona import Persona

class ProyectoXPersona(db.Model):
    id = db.Column(db.Integer,primary_key = True)
    idProyecto = db.Column(db.ForeignKey(Proyecto.id))
    idPersona = db.Column(db.ForeignKey(Persona.id))
    esAnfitrion = db.Column(db.Boolean)
    monto = db.Column(db.Float)

    def to_json(self):
        return {
            'id' : self.id,
            'idProyecto' : self.idProyecto,
            'idPersona' : self.idPersona,
            'esAnfitrion' : self.esAnfitrion,
            'monto' : self.monto
        }