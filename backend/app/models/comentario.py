from . import db
from sqlalchemy import func
from app.models.proyecto import Proyecto
from app.models.persona import Persona

class Comentario(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    idProject = db.Column(db.ForeignKey(Proyecto.id))
    idPersona = db.Column(db.ForeignKey(Persona.id))
    textoComentario = db.Column(db.String(250))
    fechaRegistro = db.Column(db.DateTime,default = func.current_timestamp())
    
    def to_json(self):
        return {
            'id' : self.id,
            'idProject' : self.idProject,
            'idPersona' : self.idPersona,
            'textoComentario' : self.textoComentario,
            'fechaRegistro' : self.fechaRegistro.__str__()
        }
    