from app.models import db
from typing import Dict,List
from sqlalchemy import *
from app.models.persona import Persona

class PersonaDataAccess:

    def get_all() -> List[Persona]:
        try:
            return Persona.query.all()
        except Exception as e:
            raise e

    def get_one(id_persona : int) -> Persona:
        try:
            return Persona.query.get(id_persona)
        except Exception as e:
            raise e