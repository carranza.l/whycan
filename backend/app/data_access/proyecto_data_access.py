from app.models import db
from typing import Dict,List,Union
from sqlalchemy import *
from app.models.proyecto import Proyecto
from app.models.proyectoXPersona import ProyectoXPersona
from app.models.problematica import Problematica
from app.models.categoria import Categoria
from app.models.departamento import Departamento
from app.models.region import Region
from app.models.persona import Persona

class ProyectoDataAccess:

    def get_one(id_proyecto : int) -> Proyecto :
        try:
            return Proyecto.query.get(id_proyecto)
        except Exception as e:
            raise e 

    def get_all() -> List[Proyecto]:
        try:
            return Proyecto.query.all()
        except Exception as e:
            raise e  

    def get_list_by_problematica(id_problematica : int) -> List[Proyecto]:
        try:
            return Proyecto.query.filter_by(idProblematica = id_problematica).order_by(Proyecto.numVotos.desc()).all()
        except Exception as e:
            raise e 

    def add_one(p : Proyecto,id_persona : int) -> bool:
        try:
            p : Proyecto
            db.session.add(p)
            db.session.flush()
            
            pXper : ProyectoXPersona
            pXper = ProyectoXPersona()
            pXper.esAnfitrion = True
            pXper.idPersona = id_persona
            pXper.idProyecto = p.id
            pXper.monto = p.recaudacion
            db.session.add(pXper)
            db.session.flush()
            return True
        except Exception as e:
            raise e

    def add_vote(id_proyecto : int) -> bool:
        try:
            p : Proyecto
            p = Proyecto.query.get(id_proyecto)
            p.numVotos = p.numVotos + 1
            db.session.add(p)
            db.session.flush()
            return True
        except Exception as e:
            raise e

    def add_donacion(id_proyecto : int ,id_persona : int ,donacion : float) -> bool:
        try:
            p : Proyecto
            p = Proyecto.query.get(id_proyecto)
            p.recaudacion = p.recaudacion + donacion

            pXper : ProyectoXPersona
            pXper = ProyectoXPersona.query.filter_by(idProyecto = id_proyecto).filter_by(idPersona = id_persona).first()
            
            if pXper != None:
                pXper.monto = pXper.monto +  donacion
            else:
                pXper = ProyectoXPersona(
                    idProyecto = id_proyecto,
                    idPersona = id_persona,
                    esAnfitrion = False,
                    monto = donacion
                )
                p.participantes = p.participantes + 1 
            db.session.add(p)
            db.session.flush()
            db.session.add(pXper)
            db.session.flush()
            return True
        except Exception as e:
            raise e

    def add_partipacion(id_proyecto : int, id_persona : int) -> bool:
        try:
            pXper : ProyectoXPersona
            pXper = ProyectoXPersona(
                idProyecto = id_proyecto,
                idPersona = id_persona,
                esAnfitrion = False,
                monto = 0
            )
            db.session.add(pXper)
            db.session.flush()
            return True
        except Exception as e:
            raise e
        
    def get_proyectos_by_categoria(id_categoria : int ) -> List[Proyecto]:
        try:
            aux = db.session.query(Proyecto).join(Problematica).join(Categoria).filter(Categoria.id == id_categoria).order_by(Proyecto.numVotos.desc()).all()
            l = []
            if aux !=None :
                for p in aux:
                    l.append(p)
            return l
        except Exception as e:
            raise e

    def get_proyectos_by_problematica(id_problematica : int) -> List[Proyecto]:
        try:
            return db.session.query(Proyecto).join(Problematica).filter(Problematica.id == id_problematica).order_by(Proyecto.numVotos.desc()).all()
        except Exception as e:
            raise e

    def get_mejores_proyectos() -> List[Union[Proyecto,Problematica,Categoria,Departamento,Region]] :

        try :
            return db.session.query(Proyecto,Problematica,Categoria,Departamento,Region).join(Problematica,Proyecto.idProblematica == Problematica.id).join(Categoria,Problematica.idCategoria == Categoria.id).join(Departamento,Problematica.idDepartamento == Departamento.id).join(Region,Departamento.idRegion == Region.id).order_by(Proyecto.numVotos.desc()).all()
        except Exception as e:
            raise e

    def get_contact(id_proyect : int) -> Persona:

        try:
            return db.session.query(Persona).join(ProyectoXPersona).filter(and_(ProyectoXPersona.idProyecto == id_proyect,ProyectoXPersona.esAnfitrion == True )).first()
        except Exception as e:
            raise e 

    