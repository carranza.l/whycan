from app.models import db
from typing import Dict,List
from sqlalchemy import *
from app.models.problematica import Problematica

class ProblematicaDataAccess:

    def get_one(id_problematica : int) -> Problematica:
        try:
            return Problematica.query.get(id_problematica)
        except Exception as e:
            raise e

    def get_all() -> List[Problematica]:
        try:
            return Problematica.query.all()
        except Exception as e:
            raise e

    def get_list_by_categoria(id_categoria : int) -> List[Problematica]:
        try:
            return Problematica.query.filter_by(idCategoria = id_categoria).all()
        except Exception as e:
            raise e 

    def get_list_by_departamento(id_departamento : int) -> List[Problematica]:
        try:
            return Problematica.query.filter_by(idDepartamento = id_departamento).all()
        except Exception as e:
            raise e

    def get_list_by_departamento_by_categoria(id_departamento : int,id_categoria : int) -> List[Problematica]:
        try:
            return Problematica.query.filter_by(idDepartamento = id_departamento).filter_by(idCategoria = id_categoria).all()
        except Exception as e:
            raise e   

    def add_one(p : Problematica) -> bool:
        try:
            db.session.add(p)
            db.session.flush()
        except Exception as e:
            raise e

        