from app.models import db
from typing import Dict,List
from sqlalchemy import *
from app.models.organizacion import Organizacion
from app.models.proyecto import Proyecto
from app.models.proyectoXOrganizacion import ProjectXOrganizacion
from app.models.organizacion import Organizacion

class ProyectoXOrganizacionDataAccess:

    def get_all_by_proyecto(id_proyecto : int) -> List[ProjectXOrganizacion]:
        try:
            return ProjectXOrganizacion.query.filter_by(idProyecto = id_proyecto).all()
        except Exception as e:
            raise e 

    def get_all_by_organizacion(id_organizacion : int) -> List[ProjectXOrganizacion]:
        try:
            return ProjectXOrganizacion.query.filter_by(idOrganizacion = id_organizacion).all()
        except Exception as e:
            raise e 

    def get_all_by_proyecto_by_organizacion(id_organizacion : int,id_proyecto : int) -> List[ProjectXOrganizacion]:
        try:
            return ProjectXOrganizacion.query.filter_by(idOrganizacion = id_organizacion).filter_by(idProyecto = id_proyecto).all()
        except Exception as e:
            raise e 

    def get_all_Organizacion_by_proyecto(id_proyecto : int) -> List[Organizacion]:
        try:
            l =  db.session.query(ProjectXOrganizacion,Organizacion ).join(Organizacion).filter(ProjectXOrganizacion.idProyecto == id_proyecto).all()
            aux = []
            if l != None:
                for _,org in l:
                    aux.append(org)
            return aux
        except Exception as e:
            raise e

    def add_patrocinador(id_proyecto : int ,id_organizacion : int) -> bool:
        try:
            exits = ProjectXOrganizacion.query.filter_by(idProyecto = id_proyecto).filter_by(idOrganizacion = id_organizacion).first()
            if exits == None:
                aux : ProjectXOrganizacion
                aux = ProjectXOrganizacion(
                    idProyecto = id_proyecto,
                    idOrganizacion = id_organizacion
                )
                db.session.add(aux)
                db.session.flush()            
                return True
            else:
                return False
        except Exception as e:
            raise e