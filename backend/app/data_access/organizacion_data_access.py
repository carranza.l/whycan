from app.models import db
from typing import Dict,List
from sqlalchemy import *
from app.models.organizacion import Organizacion

class OrganizacionDataAccess:

    def get_all() -> List[Organizacion]:
        try:
            return Organizacion.query.all()
        except Exception as e:
            raise e 

    def get_one(id_organizacion) -> Organizacion:
        try:
            return Organizacion.query.get(id_organizacion)
        except Exception as e:
            raise e