from app.models import db
from typing import Dict,List
from sqlalchemy import *
from app.models.proyectoXPersona import ProyectoXPersona

class ProyectoXPersonaDataAccess:

    def get_all_by_proyecto(id_proyecto):
        try:
            return ProyectoXPersona.query.filter_by(idProyecto = id_proyecto).all()
        except Exception as e:
            raise e
        
    def get_all_by_persona(id_persona):
        try:
            return ProyectoXPersona.query.filter_by(idPersona = id_persona).all()
        except Exception as e:
            raise e

    def get_one_by_proyecto_by_persona(id_persona : int , id_proyecto : int):
        try:
            return ProyectoXPersona.query.filter_by(idPersona = id_persona).filter_by(idProyecto = id_proyecto).all()
        except Exception as e:
            raise e