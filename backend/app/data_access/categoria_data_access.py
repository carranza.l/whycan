from app.models import db
from typing import Dict,List
from sqlalchemy import *
from app.models.categoria import Categoria

class CaetegoriaDataAccess:

    def get_all() -> List[Categoria]:
        try:
            return Categoria.query.all()
        except Exception as e:
            raise e