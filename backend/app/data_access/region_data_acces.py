from app.models import db
from typing import Dict,List
from sqlalchemy import *
from app.models.region import Region

class RegionDataAccess:

    def get_all() -> List[Region]:
        try :
            return Region.query.all()
        except Exception as e:
            raise e

