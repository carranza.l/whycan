from app.models import db
from typing import Dict,List
from sqlalchemy import *
from app.models.comentario import Comentario

class ComentarioDataAccess:

    def get_all_by_proyecto(id_proyecto : int) -> List[Comentario]:
        try:
            return Comentario.query.filter_by(idProyecto = id_proyecto).all()
        except Exception as e:
            raise e