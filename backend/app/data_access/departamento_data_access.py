from app.models import db
from typing import Dict,List
from sqlalchemy import *
from app.models.departamento import Departamento

class DepartamentoDataAccess:

    def get_all_by_region(id_region : int) -> Departamento:

        try:
            return Departamento.query.filter_by(idRegion = id_region).all()
        except Exception as e:
            raise e