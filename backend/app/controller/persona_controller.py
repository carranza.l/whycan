from app.resource import Rpta
from app.data_access import transactional
from app.data_access.persona_data_access import PersonaDataAccess

class PersonaController:

    @transactional
    def get_one(id_persona : int) -> Rpta:
        ans = Rpta()
        personaModel = PersonaDataAccess.get_one(id_persona)
        ans.setOk('Se obtuvo uno correctamente')
        ans.setBody(personaModel.to_json())
        return ans 