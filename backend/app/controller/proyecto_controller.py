from app.resource import Rpta
from app.models.proyecto import Proyecto
from app.data_access import transactional
from app.data_access.proyecto_data_access import ProyectoDataAccess
from app.data_access.categoria_data_access import CaetegoriaDataAccess
from app.data_access.problematica_data_access import ProblematicaDataAccess
from app.data_access.proyectoXOrganizacion_data_access import ProyectoXOrganizacionDataAccess


class ProyectoController:

    @transactional
    def add_one(id_problematica : int,id_persona :int ,nombre : str,descripcion : str,url_video : str,recaudacion = 0) -> Rpta:
        ans = Rpta()
        p = Proyecto(
            nombre = nombre,
            estado = 'Registrado',
            descripcion = descripcion,
            numVotos =0,
            recaudacion = 0,
            participantes = 1,
            idProblematica = id_problematica,
            recaudacionEsperada = recaudacion,
            enlaceVideo = url_video
        )
        r = ProyectoDataAccess.add_one(p,id_persona)
        ans.setBody(r)
        ans.setOk('Se agrego correctamente')
        return ans

    @transactional
    def add_vote(id_proyecto : int) -> Rpta:
        ans = Rpta()
        r = ProyectoDataAccess.add_vote(id_proyecto)
        ans.setBody(r)
        ans.setOk('Se agrego correctamente')
        return ans

    @transactional
    def add_donacion(id_proyecto : int, id_problematica : int, donacion : float) -> Rpta:
        ans = Rpta()
        r = ProyectoDataAccess.add_donacion(id_proyecto,id_problematica,donacion)
        ans.setBody(r)
        ans.setOk('Se dono correctamente')
        return ans

    @transactional
    def add_partipacion(id_proyecto : int , id_persona : int) -> Rpta:
        ans = Rpta()
        r = ProyectoDataAccess.add_partipacion(id_proyecto,id_persona)
        ans.setBody(r)
        ans.setOk('Se registro participacion correctamente')
        return ans

    @transactional
    def get_mejores_proyecto_por_categoria(id_categoria : int) -> Rpta:
        ans = Rpta()

        l_proyectoModel = ProyectoDataAccess.get_proyectos_by_categoria(id_categoria)

        l_proyecto = []
        if l_proyectoModel != None:
            for proyectoModel in l_proyectoModel:
                l_proyecto.append(proyectoModel.to_json())
        

        ans.setOk('Listo mejores mejores proyectos por  categoria')
        ans.setBody(l_proyecto)
        return ans

    @transactional
    def get_mejores_proyecto_por_problematica(id_problematica : int) -> Rpta:
        ans = Rpta()

        l_proyectoModel = ProyectoDataAccess.get_list_by_problematica(id_problematica)
        l_proyecto = []
        if l_proyectoModel != None:
            for proyectoModel in l_proyectoModel:
                l_proyecto.append(proyectoModel.to_json())


        ans.setOk('Listo mejores mejores proyectos por cada problematica')
        ans.setBody(l_proyecto)
        return ans

    @transactional
    def get_mejores_proyectos() -> Rpta:
        ans = Rpta()

        query = ProyectoDataAccess.get_mejores_proyectos()
        l = []
        for proyecto,problematica,categoria,departamento,region in query:
            proyectoDict = proyecto.to_json()
            proyectoDict['Problematica'] = problematica.to_json()
            proyectoDict['Categoria'] = categoria.to_json()
            proyectoDict['Region'] = region.to_json()
            proyectoDict['Departamento'] = departamento.to_json()
            l.append(proyectoDict)
        ans.setBody(l)
        ans.setOk('Listado correctamente')
        return ans

    @transactional
    def get_one(id_proyecto : int) ->Rpta:
        ans = Rpta()
        proyectoModel = ProyectoDataAccess.get_one(id_proyecto)
        proyecto = proyectoModel.to_json()

        proyecto['lSponsors'] = [org.to_json() for org in ProyectoXOrganizacionDataAccess.get_all_Organizacion_by_proyecto(id_proyecto)]
        persona = ProyectoDataAccess.get_contact(id_proyecto)
        proyecto['Persona'] =  persona.to_json() 
        ans.setBody(proyecto)
        ans.setOk('Se obtuvo uno')
        return ans

    @transactional
    def add_patrocinador(id_proyecto : int, id_organizacion : int) -> Rpta:
        ans = Rpta()
        r = ProyectoXOrganizacionDataAccess.add_patrocinador(id_proyecto,id_organizacion)
        if r== True :
            ans.setOk('Se agrego a patrocinador')
            ans.setBody(r)
            return ans
        else:
            ans.setError('Ya esta como patrocinador')
            ans.setBody(r)
            return ans