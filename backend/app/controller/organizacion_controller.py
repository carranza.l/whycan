from app.resource import Rpta
from app.data_access import transactional
from app.data_access.organizacion_data_access import OrganizacionDataAccess

class OrganizacionController:

    @transactional
    def get_one(id_organizacion : int) -> Rpta:
        ans = Rpta()
        organizacionModel = OrganizacionDataAccess.get_one(id_organizacion)
        ans.setOk('Se obtuvo uno correctamente')
        ans.setBody(organizacionModel.to_json())
        return ans 