from app.resource import Rpta
from app.commons.utils import convertDatetime
from app.data_access import transactional
from app.data_access.proyecto_data_access import ProyectoDataAccess
from app.data_access.problematica_data_access import ProblematicaDataAccess
from app.models.problematica import Problematica

class ProblematicaController:

    @transactional
    def get_list_proyect(id_problematica : int) -> Rpta:
        ans = Rpta()
        l_proyecto = ProyectoDataAccess.get_list_by_problematica(id_problematica)
        l = []
        if l_proyecto != None:
            for proyecto in l_proyecto:
                l.append(proyecto.to_json())

        ans.setOk('Se listo correctamente')
        ans.setBody(l)
        return ans

    @transactional
    def get_lista_problematica_by_categoria(id_categoria : int) -> Rpta:
        ans = Rpta()
        l_problematica_model = ProblematicaDataAccess.get_list_by_categoria(id_categoria)
        l = []
        if l_problematica_model != None:
            for problematicaModel in l_problematica_model:
                l.append(problematicaModel.to_json())

        ans.setOk('Se listo correctamente')
        ans.setBody(l)
        return ans

    @transactional
    def get_one(id_problematica : int) -> Rpta:
        ans = Rpta()
        problematicaModel = ProblematicaDataAccess.get_one(id_problematica)
        ans.setBody(problematicaModel.to_json())
        ans.setOk('Se obtuvo correctamente')
        return ans

    @transactional
    def add_one(nombre,descripcion, url_imagen, fecha, id_departamento, id_categoria) -> Rpta:
        ans = Rpta()
        fecha = convertDatetime(fecha)
        
        problematica  = Problematica(
            nombre = nombre,
            descripcion = descripcion,
            fecha = fecha,
            idCategoria = id_departamento,
            idDepartamento = id_departamento,
            urlImagen = url_imagen
        )
        r = ProblematicaDataAccess.add_one(problematica)
        ans.setBody(r)
        ans.setOk('Se agrego correctamente')
        return ans

    @transactional
    def add_from_list(l_problematica ) -> Rpta:
        ans = Rpta()
        r = True
        if l_problematica != []:
            for problematica in l_problematica:
                aux = Problematica(
                    nombre = problematica['nombre'],
                    descripcion = problematica['descripcion'],
                    fecha = convertDatetime(problematica['fecha']),
                    idCategoria = problematica['idCategoria'],
                    idDepartamento = problematica['idDepartamento'],
                    urlImagen = problematica['urlImagen']
                )
                r = ProblematicaDataAccess.add_one(aux)
        ans.setBody(r)
        ans.setOk('Se agrego la lista de problematicas')
        return ans