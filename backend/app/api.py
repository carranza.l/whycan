from flask_restful import Api
from flask import Flask
from flask_cors import CORS, cross_origin
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

from app.models import db

from app.models.persona import Persona
from app.models.organizacion import Organizacion
from app.models.region import Region
from app.models.categoria import Categoria
from app.models.departamento import Departamento
from app.models.problematica import Problematica
from app.models.proyecto import Proyecto
from app.models.proyectoXPersona import ProyectoXPersona
from app.models.comentario import Comentario
from app.models.proyectoXOrganizacion import ProjectXOrganizacion

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

from app.resource.organizacion_resource import *
from app.resource.persona_resource import *
from app.resource.categoria_resource import *
from app.resource.problematica_resource import *
from app.resource.proyecto_resource import *

api.add_resource(PersonaResourceGetOne,'/api/persona/getOne')
api.add_resource(OrganizacionResourceGetOne,'/api/organizacion/getOne')
api.add_resource(CategoriaGetAll,'/api/categoria/list')
api.add_resource(ProblematicaListaProyecto, '/api/problematica/proyecto/lista')
api.add_resource(ProblematicaListaByCategoria,'/api/problematica-by-categoria/lista')
api.add_resource(ProyectoAddOne,'/api/proyecto/addOne')
api.add_resource(ProyectoAddVote,'/api/proyecto/addVote')
api.add_resource(ProyectoAddDonacio,'/api/proyecto/addDonacion')
api.add_resource(ProyectoAddParticipacion,'/api/proyecto/addParticipacion')
api.add_resource(ProyectoGetMejoresProyectoByCategorias,'/api/mejores-proyectos-por-categorias')
api.add_resource(ProyectoGetMejoresProyectoByProblematica,'/api/mejores-proyectos-por-problematica')
api.add_resource(ProyectoGetMejoresProyectos,'/api/mejores-proyectos')
api.add_resource(ProyectoGetOne,'/api/proyecto/getOne')
api.add_resource(ProyectoAddPatrocinador,'/api/proyecto/addParticinador')
api.add_resource(ProblematicaAddOne,'/api/problematica/addOne')
